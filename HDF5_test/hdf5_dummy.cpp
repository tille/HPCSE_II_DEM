#include <string>
#include <iostream>
#include <random>
#include "H5Cpp.h"


const std::string FileName("Particles.h5");
const std::string DatasetName("Particle");
const std::string member_x("x");
const std::string member_y("y");
const std::string member_radius("radius");
const std::string member_color("color");

typedef struct {
    float x;
    float y;
    int radius;
    int color;
} Particle;



void initialize (Particle* particle_list, const int NUM_PARTICLES) {

    std::mt19937 mt(42);
    std::uniform_real_distribution<float> rand(0.0f, 10.0f);

    for (int i = 0; i < NUM_PARTICLES; ++i) {
        particle_list[i].x = rand(mt);
        particle_list[i].y = rand(mt);
        particle_list[i].radius = 1;
        particle_list[i].color = 1;
    }
}

int main() {

    int NUM_PARTICLES = 10000;
    int MAX_ITERATIONS = 100;

    Particle particle_list[NUM_PARTICLES]; 
    initialize(particle_list, NUM_PARTICLES);
   
    hsize_t dim[1];
    dim[0] = NUM_PARTICLES;

    int rank = sizeof(dim) / sizeof(hsize_t);

    // defining the datatype to pass HDF55
    H5::CompType mtype(sizeof(Particle));
    mtype.insertMember(member_x, HOFFSET(Particle, x), H5::PredType::NATIVE_FLOAT);
    mtype.insertMember(member_y, HOFFSET(Particle, y), H5::PredType::NATIVE_FLOAT);
    mtype.insertMember(member_radius, HOFFSET(Particle, radius), H5::PredType::NATIVE_INT);
    mtype.insertMember(member_color, HOFFSET(Particle, color), H5::PredType::NATIVE_INT);
    
    // preparation of a dataset and a file.

    H5::DataSpace space(rank, dim);
    H5::H5File *file = new H5::H5File(FileName, H5F_ACC_TRUNC);


    for (int i = 0; i < MAX_ITERATIONS; ++i) {
        H5::DataSet *dataset = new H5::DataSet(file->createDataSet(DatasetName + std::to_string(i), mtype, space));
        dataset->write(particle_list, mtype);
        initialize(particle_list,NUM_PARTICLES);
        delete dataset;
    }
    
    delete file;
    return 0;
}