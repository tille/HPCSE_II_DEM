#ifndef GRID_HPP
#define GRID_HPP

#include <list>
#include <vector>
#include <array>
#include "types.hpp"
#include "collision.hpp"
#include "PhaseSpace.hpp"
#include "utilities.hpp"


using namespace std::placeholders;

struct GridCell {

    std::list<unsigned> particles;

    idx_type idx_x;
    idx_type idx_y;

    bool operator==(const GridCell& cell) const {
        return idx_x == cell.idx_x && idx_y == cell.idx_y;
    }

    bool operator!=(const GridCell& cell) const { return !(*this == cell); }
};


struct Grid {

    const calc_t box_dimension;
    const calc_t size_tiles;

    idx_type Nx;
    idx_type Ny;

    std::vector<GridCell> cells;

	Grid(const calc_t box_dimension_, const calc_t size_tiles_)
	: box_dimension(box_dimension_),
	  size_tiles(size_tiles_),
	  Nx(ceil(box_dimension_/size_tiles_)),
	  Ny(ceil(box_dimension_/size_tiles_)),
	  cells(ceil(box_dimension_/size_tiles_)*ceil(box_dimension_/size_tiles_))
	{
	    for (idx_type ix = 0; ix < Nx; ix++) {
	        for (idx_type iy = 0; iy < Ny; iy++) {
	            get_by_index(ix, iy).idx_x = ix;
	            get_by_index(ix, iy).idx_y = iy;
	        }
	    }
	}

    void add_collisions(std::vector<calc_t>& x, std::vector<calc_t>& y, CollisionList& collision_list, const calc_t radius) {
        // iterate over all grid cells
        for (int ix = 0; ix < Nx; ix++) {
            for (int iy = 0; iy < Ny; iy++) {
                // iterate over all particles in this cell
                for (auto& index: get_by_index(ix, iy).particles) {
                    
                    // iterate over all neighboring cells
                    for (int8_t offset_x = -1; offset_x <= 1; offset_x++) {
                        if (ix+offset_x < 0 || ix+offset_x >= Nx) continue;

                        for (int8_t offset_y = -1; offset_y <= 1; offset_y++) {
                            if (iy+offset_y < 0 || iy+offset_y >= Ny) continue;

                            std::function<bool(unsigned)> pred = [&index,&x,&y,&radius] 
                            (unsigned neighbor_index) {
                            	if (index != neighbor_index)
                                	return ((2*radius - distance(x[index],y[index],x[neighbor_index],y[neighbor_index])) > 0);
                               	else {
                               		return false;
                               	}
                            };

                            neighbour_container_t neighbours = boost::adaptors::filter(get_by_index(ix+offset_x, iy+offset_y).particles,pred);

                            // apply interaction function
                            for (unsigned& neighbor_index : neighbours) {
                                // iterate over all neighbours
                                if (index != neighbor_index) {
                                	collision_list.add_collision(index, neighbor_index,2*radius - distance(x[index],y[index],x[neighbor_index],y[neighbor_index]));
                                }
                            }
                            
                        }
                    }
                }
            }
        }
    }

    void update(std::vector<calc_t>& x, std::vector<calc_t>& y) {
        std::vector<unsigned> moved_particles;

        // iterate over all grid cells
        for (idx_type ix = 0; ix < Nx; ix++) {
            for (idx_type iy = 0; iy < Ny; iy++) {
                get_by_index(ix, iy).particles.remove_if(
                    [this,&moved_particles,&ix,&iy,&x,&y] (unsigned index) {
                        bool cell_changed = get_by_index(ix,iy) != get_by_coordinates(x[index],y[index]);
                        if (cell_changed) {
                            moved_particles.push_back(index);
                        }
                        return cell_changed;
                    }
                );
            }
        }
        // iterate over all moved particles and add them to the appropiate cells
        for (unsigned& index : moved_particles) {
            get_by_coordinates(x[index],y[index]).particles.push_back(index);
        }
    }
    void push_back(const calc_t x, const calc_t y, const unsigned index) {
        get_by_coordinates(x,y).particles.push_back(index);
    }

    GridCell& get_by_coordinates(const calc_t x, const calc_t y) {
        idx_type idx_x;
        idx_type idx_y;
        idx_x = (idx_type) std::min<calc_t>(std::max<calc_t>(floor((x+box_dimension/2.0)/size_tiles), 0), Nx-1); 
        idx_y = (idx_type) std::min<calc_t>(std::max<calc_t>(floor((y+box_dimension/2.0)/size_tiles), 0), Ny-1);
        return get_by_index(idx_x, idx_y);
    }

    GridCell& get_by_index(int ix, int iy) {
        return cells[ix + Nx*iy];
    }

};


#endif