#ifndef PARAMETERS_HPP
#define PARAMETERS_HPP

#include <string>
#include "picojson.h"
#include <fstream>
#include "types.hpp"

struct Parameters {
    calc_t particle_radius = 1.0;
    //unsigned num_particles;
    calc_t box_dimension = 10;
    calc_t h = 1e-6;
    calc_t gamma = 1e-3;
    calc_t E = 2.1e11;
    calc_t G = 793e8;
    calc_t density = 7850; // in kg/m^3

    static Parameters read(const std::string filename) {
    	Parameters params;
		picojson::value json;
		std::ifstream parameters_input;
		parameters_input.open(filename);
		parameters_input >> json;
		picojson::object& o = json.get<picojson::object>();
		params.particle_radius = o["particle_radius"].get<double>();
		//params.num_particles = (unsigned) o["num_particles"].get<double>();
		params.box_dimension = o["box_dimension"].get<double>();
		params.h = o["h"].get<double>();
	    params.gamma = o["damping_coefficient"].get<double>();
	    params.E = o["young_modulus"].get<double>();
	    params.G = o["shear_modulus"].get<double>();
		parameters_input.close();
		return params;
    }
};

#endif