#ifndef PHASESPACE_HPP
#define PHASESPACE_HPP

#include <cmath>
#include <cassert>
#include <fstream>
#include <vector>
#include <list>
#include "types.hpp"
#include <functional>
#include "collision.hpp"




struct PhaseSpace {

    
    std::vector<calc_t> x;
    std::vector<calc_t> y;
    std::vector<calc_t> u;
    std::vector<calc_t> v;
    std::vector<calc_t> rotation;
    std::vector<color_t> color;


    PhaseSpace(unsigned num_particles)  {
        x.reserve(num_particles);
        y.reserve(num_particles);
        u.reserve(num_particles);
        v.reserve(num_particles);
        rotation.reserve(num_particles);
        color.reserve(num_particles);
        
    }

    void add_particle(const calc_t xi, const calc_t yi, const calc_t ui, const calc_t vi, color_t& rgb) {
        x.push_back(xi);
        y.push_back(yi);
        u.push_back(ui);
        v.push_back(vi);
        rotation.push_back(0.0);
        color.push_back(rgb);
    }

    void add_particle(const calc_t xi, const calc_t yi, const calc_t ui, const calc_t vi) {
        color_t black = {0,0,0};
        add_particle(xi,yi,ui,vi,black);
    }

    void add_particle(calc_t xi, calc_t yi) {
        add_particle(xi, yi, 0., 0.);
    }

    void add_particle(calc_t xi, calc_t yi, color_t& rgb) {
        add_particle(xi, yi, 0., 0., rgb);
    }

    unsigned size() const {
        return x.size();
    }


    
};





#include <limits>
#include <iostream>
struct GhostPhaseSpace : PhaseSpace {
    std::list<unsigned> empty_slots;

    // todo add member function that gives the real number
    // of ghost particles (i.e. without removed ones)

    // todo add tests

    GhostPhaseSpace(unsigned num_particles) : PhaseSpace(num_particles) {}

    void remove(unsigned i) {
        // set coordinates to a high value such that
        // those ghost particles are no neighbours 
        // of regular particles
        x[i] = std::numeric_limits<calc_t>::max();
        y[i] = std::numeric_limits<calc_t>::max();
        u[i] = 0;
        v[i] = 0;
        empty_slots.push_back(i);
    }

    void compress() {
        // store number of ghost particles and number of empty slots
        //  before compression
        unsigned uncompressed_size = size();
        unsigned empty_slots_count = empty_slots.size();
        // iterators to the ghost particles that will be placed
        //  in the empty slots
        auto itr_x = x.end()+empty_slots.size()-1;
        auto itr_y = y.end()+empty_slots.size()-1;
        auto itr_u = u.end()+empty_slots.size()-1;
        auto itr_v = u.end()+empty_slots.size()-1;
        // iterator to the empty slot to be processed in
        //  current iteration
        auto empty_slot_itr = empty_slots.begin();
        
        // fill empty slots
        for (unsigned i = 0; i < empty_slots.size(); ++i) {
            // get index of the free slot
            auto current_idx = *empty_slot_itr;
            // mark slot as occupied
            empty_slots.pop_front();

            // fill slot
            x[current_idx] = *itr_x;
            y[current_idx] = *itr_y;
            u[current_idx] = *itr_u;
            v[current_idx] = *itr_v;

            // increment iterators
            ++itr_x;
            ++itr_y;
            ++itr_u;
            ++itr_v;
            ++empty_slot_itr;
        }
        
        // resize position and velocity vectors
        x.resize(uncompressed_size-empty_slots_count);
        y.resize(uncompressed_size-empty_slots_count);
        u.resize(uncompressed_size-empty_slots_count);
        v.resize(uncompressed_size-empty_slots_count);
    }
};

inline std::ostream& operator<<(std::ostream& stream, const PhaseSpace& phase_space) {
    for (unsigned i=0; i<phase_space.size(); ++i) {
        stream << phase_space.x[i] << ' ' << phase_space.y[i] << '\n';
    }
    return stream;
}





struct PhaseSpaceFactory {
    static PhaseSpace create(const calc_t box_dimension, const calc_t particle_radius) {
        unsigned number_of_particles = PhaseSpaceFactory::number_of_particles(box_dimension, particle_radius);
        PhaseSpace phase_space(number_of_particles);

        color_t rgb({0,0,255});
        unsigned num = 0;

        for (unsigned layer=0; layer< PhaseSpaceFactory::number_of_layers(box_dimension, particle_radius); ++layer) {
            calc_t offset = particle_radius*(layer%2);
            for (unsigned i=0; i<PhaseSpaceFactory::particles_per_layer(box_dimension, particle_radius, layer); ++i) {
                if (num == number_of_particles / 2) {
                    rgb = {255,0,0};
                }
                phase_space.add_particle(-box_dimension/2. + particle_radius + offset + 2.*particle_radius*i,
                                         -box_dimension/2. + particle_radius + std::sqrt(3.)*particle_radius*layer, rgb);
                ++num;
            }
        }

        assert(phase_space.size() == number_of_particles);
        return phase_space;
    }

    // number of particles for a half filled box
    static unsigned number_of_particles(const calc_t box_dimension, const calc_t particle_radius) {
        unsigned number_of_layers = PhaseSpaceFactory::number_of_layers(box_dimension, particle_radius);
        return std::ceil(number_of_layers/2.)*PhaseSpaceFactory::particles_per_layer(box_dimension, particle_radius, 0) // number of particles in odd layers (0, 2, ...)
                + std::floor(number_of_layers/2.)*PhaseSpaceFactory::particles_per_layer(box_dimension, particle_radius, 1); // number of particles in even layers (1, 3, ...)
    }

    // number of particles in the `layer`th layer
    static unsigned particles_per_layer(const calc_t box_dimension, const calc_t particle_radius, const unsigned layer) {
        calc_t offset = particle_radius*(layer%2);
        return static_cast<unsigned>((box_dimension-2*offset)/(2.*particle_radius));
    }

    // number of layers for a half filled box
    static unsigned number_of_layers(const calc_t box_dimension, const calc_t particle_radius) {
        return static_cast<unsigned>((box_dimension / (2. * particle_radius) - 2.) / std::sqrt(3.) + 1);
    }
};

#endif