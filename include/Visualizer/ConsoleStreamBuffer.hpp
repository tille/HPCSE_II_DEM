#ifndef VISUALIZER_CONSOLESTREAMBUFFER_HPP
#define VISUALIZER_CONSOLESTREAMBUFFER_HPP

#include <streambuf>
#include <QObject>
#include <QPlainTextEdit>

class ConsoleStreamBuffer : QObject, public std::basic_streambuf<char> {
	Q_OBJECT
public:
	ConsoleStreamBuffer(std::ostream &stream, QPlainTextEdit* textedit_widget_) : m_stream(stream), textedit_widget(textedit_widget_) {
		connect(this, SIGNAL(signalAppend(const QString &)),
                textedit_widget_, SLOT(appendPlainText(const QString &)));

		m_old_buf = stream.rdbuf();
		stream.rdbuf(this);
	}
 	~ConsoleStreamBuffer() {
		// output anything that is left
		if (!m_string.empty())
			emit signalAppend(m_string.c_str());

		m_stream.rdbuf(m_old_buf);
	}

protected:
	virtual std::basic_streambuf<char>::int_type overflow(int_type v) {
		if (v == '\n') {
			emit signalAppend(m_string.c_str());
			m_string.erase(m_string.begin(), m_string.end());
		}
		else {
			m_string += v;
		}

		return v;
	}

	virtual std::streamsize xsputn(const char* p, std::streamsize n){
		m_string.append(p, p + n);

		int pos = 0;
		while (pos != std::string::npos) {
			pos = m_string.find('\n');
			if (pos != std::string::npos) {
				std::string tmp(m_string.begin(), m_string.begin() + pos);
				emit signalAppend(tmp.c_str());
				m_string.erase(m_string.begin(), m_string.begin() + pos + 1);
			}
		}

		return n;
	}

signals:
	void signalAppend(const QString & s);

private:
	std::ostream &m_stream;
	std::streambuf* m_old_buf;
	std::string m_string;

	QPlainTextEdit* textedit_widget;
};

#endif