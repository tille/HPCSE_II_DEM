#ifndef VISUALIZER_MAINWINDOW_HPP
#define VISUALIZER_MAINWINDOW_HPP

#include <QWidget>
#include <QPlainTextEdit>
#include <QTableWidget>
#include <QHeaderView>

#include "Visualizer/OpenGLWidget.hpp"
#include "Visualizer/ConsoleStreamBuffer.hpp"
#include "Visualizer/QSimulation.hpp"

namespace Visualizer {

class MainWindow : public QWidget {
	Q_OBJECT
public:
	MainWindow() : QWidget() {
		qRegisterMetaType< calc_t >( "calc_t" );

		// add simulation object
		simulation = new QSimulation;

		// set title
		window()->setWindowTitle("HPCSE - Discrete Element Method");

		// initialize OpenGL widget to render the simulation (particles, box)
		opengl_widget = new OpenGLWidget(simulation);
		// initialize sidebar
		sidebar = new SidebarWidget(simulation, this);
		sidebar->setMinimumWidth(200);
		sidebar->setSizePolicy(QSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum));

		// initialize console
		console = new QPlainTextEdit(this);
		console->setReadOnly(true);
		// redirect std::out to stream buffer
		//console_stream_buffer = new ConsoleStreamBuffer(std::cout, console);

		// add opengl widget and sidebar
		auto main_widget = new QWidget(this);
		auto main_layout = new QHBoxLayout(this);
		main_layout->addWidget(opengl_widget);
		main_layout->addWidget(sidebar);
		main_widget->setLayout(main_layout);
		main_layout->setContentsMargins(0, 0, 0, 0);

		auto layout = new QVBoxLayout(this);
		layout->addWidget(main_widget);
		layout->addWidget(console);

		// initialize phase space viewer (new window)
		phase_space_viewer = new TableWidget(0, 4);
		phase_space_viewer->show();
		phase_space_viewer->window()->setWindowTitle("Koordinaten");
		phase_space_viewer->setHorizontalHeaderItem(0, new QTableWidgetItem("x"));
		phase_space_viewer->setHorizontalHeaderItem(1, new QTableWidgetItem("y"));
		phase_space_viewer->setHorizontalHeaderItem(2, new QTableWidgetItem("u"));
		phase_space_viewer->setHorizontalHeaderItem(3, new QTableWidgetItem("v"));
		for (int c = 0; c < phase_space_viewer->horizontalHeader()->count(); ++c) {
		    phase_space_viewer->horizontalHeader()->setSectionResizeMode(c, QHeaderView::Stretch);
		}

		connect(simulation, SIGNAL(infrequent_stepped(calc_t)),
                this, SLOT(update_phase_space_viewer()), Qt::QueuedConnection);

		connect(simulation, SIGNAL(phase_space_changed()),
                this, SLOT(update_phase_space_viewer()), Qt::QueuedConnection);

		setLayout(layout);

		layout->setContentsMargins(0, 0, 0, 0);
	}

	virtual ~MainWindow() {
		delete opengl_widget;
		delete sidebar;
	}
signals:
	void signal_simultion_step();
public slots:
	void update_phase_space_viewer() {
		phase_space_viewer->show();
		// get phase_space
		auto phase_space_ptr = simulation->phase_space();
		// free items
		if (phase_space_ptr->size() < phase_space_viewer->rowCount()) {
			for (unsigned i=phase_space_ptr->size(); i<phase_space_viewer->rowCount(); ++i) {
				if (phase_space_viewer->item(i, 0)) delete phase_space_viewer->item(i, 0);
				if (phase_space_viewer->item(i, 1)) delete phase_space_viewer->item(i, 1);
				if (phase_space_viewer->item(i, 2)) delete phase_space_viewer->item(i, 2);
				if (phase_space_viewer->item(i, 3)) delete phase_space_viewer->item(i, 3);
			}
		}
		// add missing item
		if (phase_space_ptr->size() > phase_space_viewer->rowCount()) {
			for (unsigned i=phase_space_viewer->rowCount(); i<phase_space_ptr->size(); ++i) {
				phase_space_viewer->insertRow(i);
				phase_space_viewer->setItem(i, 0, new QTableWidgetItem());
				phase_space_viewer->setItem(i, 1, new QTableWidgetItem());
				phase_space_viewer->setItem(i, 2, new QTableWidgetItem());
				phase_space_viewer->setItem(i, 3, new QTableWidgetItem());
			}
			phase_space_viewer->resize(phase_space_viewer->sizeHint());
		}
		// set number of rows
		phase_space_viewer->setRowCount(phase_space_ptr->size());
		// draw columns
		for (unsigned i=0; i<phase_space_viewer->rowCount(); ++i) {
			phase_space_viewer->item(i, 0)->setText(QString::number(phase_space_ptr->x[i]));
			phase_space_viewer->item(i, 1)->setText(QString::number(phase_space_ptr->y[i]));
			phase_space_viewer->item(i, 2)->setText(QString::number(phase_space_ptr->u[i]));
			phase_space_viewer->item(i, 3)->setText(QString::number(phase_space_ptr->v[i]));
		}
	};
protected:
	class TableWidget : public QTableWidget {
	public:
		TableWidget(int rows, int columns, QWidget * parent = 0) : QTableWidget(rows, columns, parent) {}
		void closeEvent (QCloseEvent *event) override {
			hide();
			event->ignore();
		}

		QSize minimumSizeHint() const override {
	        QSize size(QTableWidget::sizeHint());
	        int width = 0;
	        for (int a = 0; a < columnCount(); ++a) {
	            width += columnWidth(a)+5;
	        }
	        size.setWidth(width + (columnCount() * 1));
	        int height = horizontalHeader()->height() + std::min(15, rowCount()) * rowHeight(0);
	        size.setHeight(height + (rowCount() * 1));
	        return size;
	    }
	    QSize sizeHint() const override {
	        return minimumSizeHint();
	    }
	};

	// OpenGL widget to render the simulation (particles, box)
	OpenGLWidget* opengl_widget;

	// SideBar widget (controls, initial configuration selector, parameter selector)
	SidebarWidget* sidebar;

	QTableWidget* phase_space_viewer;

	QPlainTextEdit* console;

	ConsoleStreamBuffer* console_stream_buffer;

	// Simulation object (stores parameters, phase space, ghost phase space)
	QSimulation* simulation;
};

} // end ns Visualizer

#endif