#ifndef VISUALIZER_QOPENGLWIDGET_HPP
#define VISUALIZER_QOPENGLWIDGET_HPP

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QGLFormat>
#include <QMouseEvent>
#include <QMetaMethod>


#include "Visualizer/QSimulation.hpp"

namespace Visualizer {

class OpenGLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
	//using color_t = std::array<float, 3>;

    OpenGLWidget(QSimulation* s);
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void paintEvent(QPaintEvent*) override;
    QColor& foreground_color() { return m_foreground_color; }
    QColor& background_color() { return m_background_color; }

    QSize sizeHint() const override {
    	return QSize(500, 400);
    }

    struct Viewport {
	  int x0 = 0;
	  int y0 = 0;
	  int width;
	  int height;
	  float zoom = 1;

	  void translate(int x, int y) {
	    x0 -= x;
	    y0 -= y;
	  }
	};

	// viewport instance
	Viewport viewport;

	struct Mouse {
	  int drag_x;
	  int drag_y;
	  bool drag;
	};

	// mouse instance
	Mouse mouse;

	QSimulation* simulation;

	void wheelEvent ( QWheelEvent * event ) override {
		auto delta = event->delta()/100.;
		if (viewport.zoom + delta > 0)
			viewport.zoom += delta;
	}
protected:
    void mousePressEvent(QMouseEvent* e) override;
    void mouseReleaseEvent(QMouseEvent*) override;
    void mouseMoveEvent(QMouseEvent* e) override;
    void updateCamera();

    unsigned number_of_triangles = 30;

    void draw_particle(calc_t x, calc_t y, unsigned radius, color_t color) {
      glColor3f(color[0], color[1], color[2]);
	  glBegin(GL_TRIANGLE_FAN);
	  glVertex2f(x, y); // midpoint
	  for (int i = 0; i <=number_of_triangles; i++){     
	      glVertex2f(x + radius*cos(i*M_PI/(number_of_triangles/2)), 
	                 y + radius*sin(i*M_PI/(number_of_triangles/2)));
	  }
	  glEnd();
    }

    QColor m_foreground_color = {255, 255, 255};
    QColor m_background_color = {  0,   0,   0};

    QOpenGLContext *m_context;

    QMetaMethod update_method;
};

} // end ns Visualizer

#endif