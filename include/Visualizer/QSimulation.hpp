#ifndef VISUALIZER_QSIMULATION
#define VISUALIZER_QSIMULATION

#include <QObject>
#include "simulation.hpp"

namespace Visualizer {

// wrapper arround simulation that enables usage of qt signals without
//  making the base class depend on qt. this is especially useful when
//  the connected slots should be run in the gui event loop
class QSimulation : public QObject, public Simulation {
	Q_OBJECT

public:
	QSimulation() : QObject(), Simulation() {
		// make sure that the signals of the base class are triggered
		connect(this, &QSimulation::stepped,
                [this] (calc_t t) { Simulation::stepped(t); });
		connect(this, &QSimulation::infrequent_stepped,
                [this] (calc_t t) { Simulation::infrequent_stepped(t); });
		connect(this, &QSimulation::started,
                [this] () { Simulation::started(); });
		connect(this, &QSimulation::stopped,
                [this] () { Simulation::stopped(); });
		connect(this, &QSimulation::restarted,
                [this] () { Simulation::restarted(); });
		connect(this, &QSimulation::phase_space_changed,
                [this] () { Simulation::phase_space_changed(); });
	}

	Q_SIGNAL void stepped(calc_t) override;
	Q_SIGNAL void infrequent_stepped(calc_t) override;
	Q_SIGNAL void started() override;
	Q_SIGNAL void stopped() override;
	Q_SIGNAL void restarted() override;
	Q_SIGNAL void phase_space_changed() override;

public slots:
	void start() override {
		Simulation::start();
	};
	void stop(bool blocking=false) override {
		Simulation::stop(blocking);
	};
	void restart() override {
		Simulation::restart();
	};
};

}

#endif