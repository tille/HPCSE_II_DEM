#ifndef VISUALIZER_SIDEBARWIDGET_HPP
#define VISUALIZER_SIDEBARWIDGET_HPP

#include <QWidget>
#include <QStyle>
#include <QAction>
#include <QToolBar>
#include <QComboBox>
#include <QGroupBox>
#include <QLineEdit>
#include <QFormLayout>
#include <QPushButton>
#include <QLabel>

#include "Visualizer/QSimulation.hpp"

namespace Visualizer {

class SidebarWidget : public QWidget {
	Q_OBJECT
public:
	SidebarWidget(QSimulation* simulation_, QWidget* parent) : QWidget(reinterpret_cast<QWidget*>(parent)) {
		simulation = simulation_;

		// create controls
		layout = new QVBoxLayout;
		layout->setAlignment(Qt::AlignTop);
		setLayout(layout);

		// display simulation state
		QGroupBox* controls_toolbar_group = new QGroupBox(QObject::tr("Simulation"));
		layout->addWidget(controls_toolbar_group);
		auto controls_toolbar_group_layout = new QVBoxLayout();
		controls_toolbar_group->setLayout(controls_toolbar_group_layout);

		// add control toolbar (play, pause, stop, restart)
		QAction* play_action = new QAction(style()->standardIcon(QStyle::SP_MediaPlay), QObject::tr("Play"), this);
		QAction* pause_action = new QAction(style()->standardIcon(QStyle::SP_MediaPause), QObject::tr("Play"), this);
		QAction* stop_action = new QAction(style()->standardIcon(QStyle::SP_MediaStop), QObject::tr("Play"), this);
		QAction* restart_action = new QAction(style()->standardIcon(QStyle::SP_BrowserReload), QObject::tr("Play"), this);
		QToolBar* controls_toolbar = new QToolBar;
		controls_toolbar_group_layout->addWidget(controls_toolbar);
		controls_toolbar->addAction(play_action);
		controls_toolbar->addSeparator();
		controls_toolbar->addAction(pause_action);
		controls_toolbar->addSeparator();
		controls_toolbar->addAction(stop_action);
		controls_toolbar->addSeparator();
		controls_toolbar->addAction(restart_action);
		connect(play_action, SIGNAL(triggered()),
                simulation, SLOT(start()));
		connect(stop_action, SIGNAL(triggered()),
                simulation, SLOT(stop()));
		connect(restart_action, SIGNAL(triggered()),
                this, SLOT(simulation_restart()));

		// display simulation state variables
		QGridLayout* simulation_state_variables_layout = new QGridLayout();
		simulation_state_variables_layout->addWidget(new QLabel(QObject::tr("t")), 0, 0);
		simulation_state_variables_layout->addWidget(time_label = new QLabel(QObject::tr("0.0")), 0, 1);
		controls_toolbar_group_layout->addLayout(simulation_state_variables_layout);

		// initial configuration select box
		QGroupBox* init_conf_gbox = new QGroupBox(QObject::tr("Initial configuration"));
		auto init_conf_gbox_layout = new QVBoxLayout;
		init_conf_gbox->setLayout(init_conf_gbox_layout);
		init_conf_select = new QComboBox();
		init_conf_gbox_layout->addWidget(init_conf_select);
		init_conf_select->addItem("Two particles");
		init_conf_select->addItem("Single particle");
		init_conf_select->addItem("Half filled");
		layout->addWidget(init_conf_gbox);

		connect(init_conf_select, SIGNAL(currentIndexChanged(int)),
                this, SLOT(select_initial_configuration(int)));

		// add parameter input form
		QGroupBox* parameter_input = new QGroupBox(QObject::tr("Parameters"));
		layout->addWidget(parameter_input);
		QFormLayout* parameter_input_layout = new QFormLayout;
		parameter_input->setLayout(parameter_input_layout);
		particle_radius_edit 	 = new QLineEdit(QString::number(simulation->params()->particle_radius), this);
		box_dimension_edit 		 = new QLineEdit(QString::number(simulation->params()->box_dimension), this);
		time_step_edit 			 = new QLineEdit(QString::number(simulation->params()->h), this);
		damping_coefficient_edit = new QLineEdit(QString::number(simulation->params()->gamma), this);
		young_modulus_edit 		 = new QLineEdit(QString::number(simulation->params()->E), this);
		shear_modulus_edit 		 = new QLineEdit(QString::number(simulation->params()->G), this);

		QDoubleValidator* validator = new QDoubleValidator(this);
		particle_radius_edit->setValidator(validator);
		box_dimension_edit->setValidator(validator);
		time_step_edit->setValidator(validator);
		damping_coefficient_edit->setValidator(validator);
		young_modulus_edit->setValidator(validator);
		shear_modulus_edit->setValidator(validator);

		parameter_input_layout->addRow(QObject::tr("r"),  particle_radius_edit);
		parameter_input_layout->addRow(QObject::tr("d"),  box_dimension_edit);
		parameter_input_layout->addRow(QObject::tr("Δt"), time_step_edit);
		parameter_input_layout->addRow(QObject::tr("γ"),  damping_coefficient_edit);
		parameter_input_layout->addRow(QObject::tr("E"),  young_modulus_edit);
		parameter_input_layout->addRow(QObject::tr("G"),  shear_modulus_edit);

		QPushButton* apply_button = new QPushButton("&Apply", this);
		parameter_input_layout->addRow(apply_button);
		connect(apply_button, &QPushButton::clicked,
                this, &SidebarWidget::applyParameters);

		// listen to MainWindow simulation step events
		connect(simulation, SIGNAL(infrequent_stepped(calc_t)),
                this, SLOT(simulation_step(calc_t)));

		// select initial configuration
		select_initial_configuration(0);
	}

	void applyParameters() {
		simulation->params()->particle_radius = particle_radius_edit->text().toDouble();
		simulation->params()->box_dimension = box_dimension_edit->text().toDouble();
		simulation->params()->h = time_step_edit->text().toDouble();
		simulation->params()->gamma = damping_coefficient_edit->text().toDouble();
		simulation->params()->E = young_modulus_edit->text().toDouble();
		simulation->params()->G = shear_modulus_edit->text().toDouble();
	}

public slots:
	void simulation_restart() {
		// reinitialize with currently selected configuration
		select_initial_configuration(init_conf_select->currentIndex());
		// restart the simulation if already running
		if (simulation->running())
			simulation->restart();
	}
	void simulation_step(calc_t t) {
		time_label->setText(QString::number(t));
	}

	void select_initial_configuration(int index) {
		if (simulation->running())
			simulation->stop();
		
		switch (index) {
			case 1: { // single particle
				// create new phase space instance
				auto phase_space = std::make_shared<PhaseSpace>(1);
				// add a single particle in the center of the box
				phase_space->add_particle(0, 0);
				// pass phase space to the simulation
				simulation->set_phase_space(phase_space);
				break;
			} case 2: { // half filled
				auto box_dimension = simulation->params()->box_dimension;
				auto particle_radius = simulation->params()->particle_radius;
				auto phase_space = std::make_shared<PhaseSpace>(PhaseSpaceFactory::create(box_dimension, particle_radius));
				simulation->set_phase_space(phase_space);
				break;
			} case 0: {
				// create new phase space instance
				auto phase_space = std::make_shared<PhaseSpace>(1);
				// add a single particle in the center of the box
				phase_space->add_particle(0, 0);
				// add a single particle in the bottom of the box
				phase_space->add_particle(0.5, -simulation->params()->box_dimension/2+simulation->params()->particle_radius);

				// pass phase space to the simulation
				simulation->set_phase_space(phase_space);
				break;
			} default:
				throw std::runtime_error("This should not happen");
		}
	}

protected:
	QVBoxLayout* layout;

	QSimulation* simulation;

	// parameter input fields
	QLineEdit* particle_radius_edit;
	QLineEdit* box_dimension_edit;
	QLineEdit* time_step_edit;
	QLineEdit* damping_coefficient_edit;
	QLineEdit* young_modulus_edit;
	QLineEdit* shear_modulus_edit;

	// simulation time
	QLabel* time_label;

	// initial configuration select box
	QComboBox* init_conf_select;
};

} // end ns Visualizer

#endif