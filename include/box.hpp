#ifndef BOX_HPP
#define BOX_HPP
#include <vector>
#include <functional>
#include <array>
#include "types.hpp"
#include "utilities.hpp"
#include "Grid.hpp"


using namespace std::placeholders;

struct Box {

	Point midpoint;
	std::vector<Point> vertices;
	std::vector<Vector2D> normals;
	calc_t box_dimension;

	Grid grid;

	Box () : midpoint(Point(0.,0.)), vertices(4), normals(4), grid(0,4) {};

	Box (calc_t _box_dimension) : box_dimension(_box_dimension), midpoint(Point(0.,0.)), grid(2*_box_dimension,4) {

		vertices.reserve(4);
		normals.reserve(4);

		vertices.push_back(Point(-_box_dimension/2.,-_box_dimension/2.));
		vertices.push_back(Point(_box_dimension/2.,-_box_dimension/2.)); 
		vertices.push_back(Point(_box_dimension/2.,_box_dimension/2.)); 
		vertices.push_back(Point(-_box_dimension/2.,_box_dimension/2.));

		normals.push_back(Vector2D(-1.,0.));
		normals.push_back(Vector2D(0.,-1.));
		normals.push_back(Vector2D(1.,0.));
		normals.push_back(Vector2D(0.,1.));
	};

	
	

	void updateCells(std::vector<calc_t>& x, std::vector<calc_t>& y) {
        grid.update(x,y);
    }

    void add_collisions(std::vector<calc_t>& x, std::vector<calc_t>& y, CollisionList& collision_list, const calc_t radius) {
        grid.add_collisions(x,y,collision_list,radius);
    } 

    void turn (calc_t phi) {

		for (int i = 0; i < 4; ++i) {
			rotate_point(vertices[i],phi);
			rotate_vector(normals[i],phi);
		}
	}

	void rotate_point (Point& p, calc_t phi) {

  		// translate point back to origin:
  		p.x -= midpoint.x;
 		p.y -= midpoint.y;

  		// rotate point
  		calc_t xnew = p.x * cos(phi) - p.y * sin(phi);
  		calc_t ynew = p.x * sin(phi) + p.y * cos(phi);

		// translate point back:
		p.x = midpoint.x + xnew;
		p.y = midpoint.y + ynew;
	}

	void rotate_vector (Vector2D& v, calc_t phi) {

  		// rotate point
  		calc_t xnew = v(0) * cos(phi) - v(1) * sin(phi);
  		calc_t ynew = v(0) * sin(phi) + v(1) * cos(phi);

		// translate point back:
		v(0) = xnew;
		v(1) = ynew;
  		
	}

	/*
	 * returns the distance of particle (x,y) to the midpoint (which is (0,0) in our case)
	 */
	calc_t distance_to_midpoint(calc_t x, calc_t y) {
		return std::sqrt(std::pow(x - midpoint.x,2) + std::pow(y - midpoint.y,2));
	}

	/*
	 * returns the distance of particle (x,y) to a line defined defined by Point A and normal vector normal
	 */
	calc_t distance_to_edge (calc_t x, calc_t y, Point A, Vector2D normal) {

		Point M(x,y);
		Vector2D AM(A-M);
		Vector2D projection((normal.dot(AM))/(normal.dot(normal)) * normal);
		return projection.norm();
	}
	 
	/*
	 * returns the minimum distance of particle (x,y) to all four sides of the box
	 */
	calc_t distance_to_border (calc_t x, calc_t y) {

		std::vector<calc_t> distances(4);
		for (unsigned int i = 0; i < 4; ++i) {
			distances[i] = distance_to_edge(x,y,vertices[i],normals[i]);
		}
		return *std::min_element(distances.begin(),distances.end());
	}


	/*
	 * returns true if particle (x,y) with radius particle_radius is outside of the box
	 * if (x,y) is inside the box but close to the border so that the circle(x,y,particle_radius)
	 * intersects the border, the function returns true
	 */

	bool is_outside (calc_t x, calc_t y, calc_t particle_radius) {

		//excludes all particles far away from the boundary
		if (distance_to_midpoint(x,y) < (box_dimension/2. - particle_radius)) {
			return false;
		}

		//checks whether particles are actually outside
		Point M(x,y);
		Vector2D AM(vertices[0] - M);
		Vector2D AB(vertices[0]-vertices[1]);
		calc_t AM_AB = AM.dot(AB);
		calc_t AB_AB = AB.dot(AB);
		if (!(0 <= AM_AB && AM_AB <= AB_AB)) {
			return true;
		}

		Vector2D AD(vertices[0]-vertices[3]);
		calc_t AM_AD = AM.dot(AD);
		calc_t AD_AD = AD.dot(AD);
		if (!(0 <= AM_AD && AM_AD <= AD_AD)) {
			return true;
		}

		//checks whether particles are inside the box but overlap with the border
		if (distance_to_border(x,y) < particle_radius) {
			return true;
		}
		return false;
	}


};


#endif