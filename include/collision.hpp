#ifndef COLLISION_HPP
#define COLLISION_HPP

#include <vector>
#include <list>
#include <algorithm>
#include "types.hpp"
#include "PhaseSpace.hpp"

struct Collision {
	unsigned i;
	unsigned j;
	calc_t vt_rel;
	unsigned num_steps;
	calc_t xin;
	//unsigned border;

	Collision(unsigned _i, unsigned _j, calc_t _xin) : i(_i), j(_j), xin(_xin) {
		num_steps = 0.0;
		vt_rel = 0.0;
	}

	void update_collision(calc_t _xin, calc_t _vt_rel) {
		++num_steps;
		xin = _xin;
		vt_rel = _vt_rel;
	}

};


struct CollisionList {

	std::vector<std::list<Collision>> collision_list;

	CollisionList(unsigned size) {
		collision_list.resize(size);
	}

	bool find_collision(unsigned i, unsigned j) {
		assert(i != j);
		return std::find_if(collision_list[i].begin(), collision_list[i].end(), [&j](Collision& element) {return element.j == j;}) != collision_list[i].end();
	}

	void add_collision(unsigned i, unsigned j, calc_t xin) {
		assert(i != j);
		if (find_collision(i,j) == false) {
			collision_list[i].emplace_back(i,j,xin);
		}
	}
	void remove_collision(unsigned i, unsigned j) {
		collision_list[i].remove_if([&](Collision element){return element.j = j;});
	}

	std::list<Collision>& operator [] (unsigned i) {
		return collision_list[i];
	}

	unsigned size() {
		return collision_list.size();
	}

};




#endif