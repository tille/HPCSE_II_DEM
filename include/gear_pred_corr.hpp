#include "types.hpp"
#include <iostream>
#include "types.hpp"

/**
 * approximate position and velocity at t+h using 3rd order
 * gear predictor corrector shema
 *
 * - the result of the approximation is stored directly in x and v
 * - note that the force history is updated, i.e. after evaluation
 *  the force at time t-h (f_tm1) is set to the force at time t (f)
 *  before evaluation
 * 
 * \param x      - position at time t
 * \param v      - position at time v
 * \param t      - time t
 * \param h      - timestep h
 * \param f      - force at time t
 * \param f_tm1  - force at time t-h
 * \param f_tm2  - force at time t-2*h
 * \param force  - force in procedual form
 * \param x_corr - approximated position at time t+h
 * \param v_corr - approximated velocity at time t+h
 */
 
template <typename F>
void gear_pred_corrector(calc_t& x, calc_t& v, const calc_t t, const calc_t h, const calc_t f, calc_t& f_tm1, calc_t& f_tm2, F force) {
    // evaluate force
    //const calc_t f = force(x, v, t);

    // predict position and velocity at t+h
    //  x_pred(t+h)
    const calc_t x_pred = x + h*(v + h*(19./24.*f - 10./24.*f_tm1 + 3./24.*f_tm2));
    //  v_pred(t+h)
    const calc_t v_pred = (x_pred-x)/h + h*(27./24.*f - 22./24.*f_tm1 + 7./24.*f_tm2);

    // evaluate force using predicted values
    const calc_t f_pred = force(x_pred, v_pred, t+h);

    const calc_t x_tmp = x;
    // correct position and velocity
    x = x_tmp+h*(v+h*(3./24.*f_pred + 10./24.*f - 1./24.*f_tm1));
    v = (x-x_tmp)/h+h*(7./24.*f_pred + 6./24.*f - 1./24.*f_tm1);

    // update force history
    f_tm2 = f_tm1;
    f_tm1 = f;
}

//calc_t force_dummy(calc_t x, calc_t v, calc_t t) {
//    return 1.;
//}
//
//template void gear_pred_corrector<decltype(force_dummy)>(calc_t&, calc_t&, calc_t, calc_t, calc_t, calc_t&, calc_t&, decltype(force_dummy));