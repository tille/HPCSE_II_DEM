#ifndef MIXING_CRITERION_HPP
#define MIXING_CRITERION_HPP

#include <vector>
#include <functional>
#include <algorithm>
#include <tuple>
#include "box.hpp"


std::tuple<double,double> compute_mean_and_standard_deviation(std::vector<int>& diff) {

	const unsigned num_tiles = diff.size();
	double mean = std::accumulate(diff.begin(),diff.end(),0.0) / num_tiles;
	double stddev = 0.0;
	for (unsigned i = 0; i < num_tiles; ++i) {
		stddev += std::pow(diff[i] - mean,2);
	}
	stddev = std::sqrt(stddev/num_tiles);
	return std::make_tuple(mean,stddev);
}

void compute_mixing_criterion(PhaseSpace& phase_space, Box& b) {

	const unsigned size = phase_space.size();
	std::vector<unsigned> dimensions({1,2,4,8,16,32,64});
	color_t blue({0,0,255});
	color_t red({255,0,0});


	for (unsigned dim = 0; dim < dimensions.size(); ++dim) {
		unsigned dimension = dimensions[dim];
		unsigned num_tiles = dimension*dimension;
		unsigned tile_size = b.box_dimension / dimension;
		std::vector<int> blue_particles_count(num_tiles,0);
		std::vector<int> red_particles_count(num_tiles,0);

		auto& x = phase_space.x;
	    auto& y = phase_space.y;
	    auto& color = phase_space.color;


		for (unsigned i = 0; i < size; ++i) {
			unsigned col = (unsigned) ((x[i] + 0.5*b.box_dimension) / tile_size);
			unsigned row = (unsigned) ((y[i] + 0.5*b.box_dimension) / tile_size);

			if (color[i] == blue)
				++blue_particles_count[dimension*row+col];
			else if (color[i] == red)
				++red_particles_count[dimension*row+col];		
		}


		std::vector<int> diff(num_tiles);
		for (unsigned i = 0; i < num_tiles; ++i) {
			diff[i] = blue_particles_count[i] - red_particles_count[i];
		}

		double mean, stddev;
		std::tie(mean,stddev) = compute_mean_and_standard_deviation(diff);
		std::cout << dimension << " " << mean << " " << stddev << std::endl;

	}
}



#endif