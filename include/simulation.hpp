#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include <vector>
#include <thread>
#include <future>
#include <chrono>
#include "types.hpp"
#include "Parameters.hpp"
#include "PhaseSpace.hpp"
#include "box.hpp"

#include <memory>

class Simulation;

void simulation (Simulation& simulation);

class Simulation {
	friend void simulation(Simulation&);
public:
	Simulation () : m_running(false) {
		m_params.reset(new Parameters);
		m_ghost_phase_space.reset(new GhostPhaseSpace(1));
		tp = std::chrono::system_clock::now();
	}

	// -------------------------------------------------------------------------
	// Getter & Setter
	// -------------------------------------------------------------------------
	std::shared_ptr<PhaseSpace> phase_space(bool unsafe=false) {
		if (!unsafe && !m_phase_space) {
			throw std::runtime_error("Access to invalid PhaseSpace pointer");
		}
		return m_phase_space;
	}

	std::shared_ptr<GhostPhaseSpace> ghost_space() {
		if (!m_ghost_phase_space) {
			throw std::runtime_error("Access to invalid GhostPhaseSpace pointer");
		}
		return m_ghost_phase_space;
	}

	std::shared_ptr<Box> box() {
		return m_box;
	};

	void set_phase_space(std::shared_ptr<PhaseSpace> phase_space) {
		m_phase_space = phase_space;
		m_box = std::make_shared<Box>(params()->box_dimension);
		phase_space_changed();
	}

	std::shared_ptr<Parameters> params() {
		return m_params;
	}

	// register callback that is called after every simulation step
	// note: qt functions must be called using signals, since the callbacks
	//  are called from a different thread then the gui thread
	void register_step_cb(std::function<void()> cb) {
		redraw_slots.push_back(cb);
	}

	// -------------------------------------------------------------------------
	// Status
	// -------------------------------------------------------------------------
	bool running() const {
		return m_running.load();
	}

	// -------------------------------------------------------------------------
	// Slots
	// -------------------------------------------------------------------------
	// todo: add blocking version
	virtual void start() {
		// check that the simulation is not running already
		if (running())
			throw std::runtime_error("Simulation was already running");

		// check that we have a valid phase space object
		if (!m_phase_space)
			throw std::runtime_error("PhaseSpace was not initialized correctly");

		// set state to running
		m_running.store(true);

		m_time = 0.;

		// join previous thread (should be non blocking since !running())
		if (m_thread && m_thread->joinable())
			m_thread->join();

		// spawn simulation thread
		//std::promise<bool> promise;
    	//m_thread_exit_future = std::make_shared<std::future<bool>>(std::move(promise.get_future()));
		m_thread = std::make_shared<std::thread>([this] () {
			// spawn simulation
			simulation(*this);
			// signal that the thread has terminated
			m_running.store(false);
			m_stop_requested = false;
		});

		started();
		//m_thread = std::make_shared<std::thread>(simulation,
		//													  std::ref(*m_phase_space),
		//													  std::ref(*m_ghost_phase_space),
		//													  std::ref(*m_params),
		//													  redraw_slots.back());
	}

	virtual void stop(bool blocking=false) {
		if (!running())
			return;

		if (blocking) {
			_stop();
		} else {
			std::async(std::launch::async, [this] () {
				_stop();
			});
		}
	}

	virtual void restart() {
		std::async(std::launch::async, [this] () {
			stop(true);
			start();
			restarted();
		});
	}

	std::chrono::time_point<std::chrono::system_clock> tp;

	// -------------------------------------------------------------------------
	// Signals
	// -------------------------------------------------------------------------
	virtual void started() {}
	virtual void stopped() {}
	virtual void restarted() {}
	virtual void stepped(calc_t time) {
		m_time = time;
		// max of 60 fps
		auto now = std::chrono::system_clock::now();
		if (std::chrono::duration<double>(now-tp).count() > 1./60) {
			infrequent_stepped(time);
			tp = now;
		}
	}
	virtual void phase_space_changed() {}

protected:
	std::shared_ptr<Parameters> m_params;

	std::shared_ptr<PhaseSpace> m_phase_space;

	std::shared_ptr<GhostPhaseSpace> m_ghost_phase_space;

	std::shared_ptr<Box> m_box;

	std::shared_ptr<std::thread> m_thread;

	std::shared_ptr<std::future<bool>> m_thread_exit_future;

	std::vector<std::function<void()>> redraw_slots;

	std::atomic<bool> m_running;

	std::chrono::time_point<std::chrono::high_resolution_clock> m_time_step_finished;

	bool m_stop_requested = false;

	calc_t m_time = 0.;

	// this signal is only triggered at most once per frame
	virtual void infrequent_stepped(calc_t t) {
		for (auto& cb : redraw_slots) {
			cb();
		}
	}

	bool stop_requested() {
		return m_stop_requested;
	}

	// blocking stop
	void _stop() {
		// request stop
		m_stop_requested = true;

		// wait until current simulation thread stopped execution
		if (m_thread && m_thread->joinable())
			m_thread->join();

		m_stop_requested=false;

		// signal
		stopped();
	}
};

#endif