#ifndef TYPES_HPP
#define TYPES_HPP

#include <array>
#include <list>
#include <boost/range/adaptor/filtered.hpp>

using calc_t = double;
using color_t = std::array<uint8_t,3>;

typedef unsigned idx_type;
typedef boost::range_detail::filtered_range<std::function<bool(unsigned)>, std::list<unsigned>> neighbour_container_t;

#endif