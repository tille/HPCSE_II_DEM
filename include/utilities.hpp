#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include "types.hpp"
#include <array>

struct Point {

    calc_t x;
    calc_t y;
    Point() : x(0.), y(0.) {};
    Point(calc_t _x, calc_t _y) : x(_x), y(_y) {};

    friend Point operator-(Point A,Point B) {
        Point p(A.x - B.x, A.y - B.y);
        return p;
    }
};

struct Vector2D {

    std::array<calc_t,2> vec;
    Vector2D () : vec{0.,0.} {};
    Vector2D (calc_t _x, calc_t _y) {
        vec[0] = _x;
        vec[1] = _y;
    };
    Vector2D(Point& A) : vec{A.x,A.y}  {};
    Vector2D(Point&& A) : vec{A.x,A.y}  {};
    Vector2D(Vector2D& v) {
        vec[0] = v.vec[0];
        vec[1] = v.vec[1];
    };
     Vector2D(Vector2D&& v) {
        vec[0] = v.vec[0];
        vec[1] = v.vec[1];
    };

    calc_t& operator() (unsigned i) {
        return vec[i];
    }

    calc_t dot(Vector2D& A) {
        return vec[0] * A(0) + vec[1] * A(1);
    }
     calc_t norm() {
        return std::sqrt(vec[0] * vec[0] + vec[1] * vec[1]);
    }

    friend Vector2D operator+(Vector2D a, Vector2D b) {
        Vector2D v(a.vec[0] + b.vec[0], a.vec[1] + b.vec[1]);
        return v;
    }
     friend Vector2D operator*(calc_t scalar, Vector2D a) {
        Vector2D v(scalar*a(0), scalar*a(1));
        return v;
    }
};



inline calc_t distance (const calc_t x1, const calc_t y1, const calc_t x2, const calc_t y2) {
    assert(x1!=x2 || y1 != y2);
    return std::sqrt( std::pow(x1-x2,2) + std::pow(y1-y2,2));
}



#endif