#include <QApplication>
#include <QPushButton>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QGLFormat>
#include <iostream>
#include <QMouseEvent>
#include <QTimer>
#include <QFormLayout>
#include <QLineEdit>
#include <QGroupBox>
#include <QMetaMethod>
#include "Visualizer/OpenGLWidget.hpp"
#include "box.hpp"

namespace Visualizer {

using namespace std::placeholders;

OpenGLWidget::OpenGLWidget(QSimulation* s) : QOpenGLWidget() {
	/*
	 * Setup opengl
	 */
    QSurfaceFormat format;
    // enable antialiasing
    format.setSamples(4);
    setFormat(format);

    m_context = new QOpenGLContext;
    m_context->setFormat(format);
    m_context->create();

    // widget must have at least this size
    setMinimumSize(300, 300);

    // store simulation handle
    simulation = s;

    // note: since we also have mouse input we use a fixed framerate
    //  if performance issues are encountered usage of a update on
    //  change paradigm might be useful
	//connect(s, SIGNAL(infrequent_stepped(calc_t)),
    //        this, SLOT(update()), Qt::QueuedConnection);

    // tell opengl that we don't want it to empty the framebuffer
    //  this is required because of the structure in 
    //  paintEvent(QPaintEvent*)
    setUpdateBehavior(QOpenGLWidget::PartialUpdate);
}

void OpenGLWidget::initializeGL() {
	initializeOpenGLFunctions();
    glEnable(GL_MULTISAMPLE); // enable multisampling
}

void OpenGLWidget::resizeGL(int width, int height) {
	// center origin (0, 0)
	viewport.x0 = -width/2;
	viewport.y0 = -height/2;
	viewport.width = width;
    viewport.height = height;
    glViewport(0, 0, width, height);
    // determine zoom
    viewport.zoom = std::min(0.9*width/simulation->params()->box_dimension,
    						 0.9*height/simulation->params()->box_dimension);
    updateCamera();
}

void OpenGLWidget::paintEvent(QPaintEvent* e) {
	// setup qt painter
	QPainter painter(this);

	// clear framebuffer
	painter.beginNativePainting();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	painter.endNativePainting();

	// set viewport
	painter.setViewport(-viewport.x0, viewport.height+viewport.y0, viewport.width, viewport.height);

	// calculate coordinate transformation
	QMatrix m;
	m.scale(viewport.zoom, viewport.zoom); // apply zoom
	m.scale(1, -1); // flip y coordinate

	/*
	 * Draw coordinate axis
	 */
	QPen pen( Qt::black );
	QPen thick_pen( Qt::black );
	thick_pen.setWidth(3);

	// horizontal axis
	painter.drawLine(viewport.x0, 0, viewport.x0+viewport.width, 0);
	// vertical axis
	painter.drawLine(0, -viewport.y0, 0, -viewport.y0-viewport.height);

	// draw ticks
	float line_width = 8.f;
	calc_t box_dimension = simulation->params()->box_dimension;
	calc_t particle_radius = simulation->params()->particle_radius;
	int tick_count = box_dimension/particle_radius;
	for(int i=-tick_count; i<=tick_count; ++i) {
		QPoint r1(0, i*particle_radius);
		QPoint r2(i*particle_radius, 0);
		// transform coordinates
		r1 = m*r1;
		r2 = m*r2;

		if (i%10 == 0) {
		  painter.setPen(thick_pen);
		  painter.drawLine(r1.x()-line_width, r1.y(), r1.x()+line_width, r1.y());
		  painter.drawLine(r2.x(), r2.y()-line_width, r2.x(), r2.y()+line_width);
		  painter.setPen(pen);
		  
		  if (i!=0) {
			  painter.drawText(r1.x()+2*line_width, r1.y(), QString::number(i));
			  painter.drawText(r2.x(), r2.y()+2*line_width, QString::number(i));
		  }
		}
		else {
		 painter.drawLine(r1.x()-line_width, r1.y(), r1.x()+line_width, r1.y());
		 painter.drawLine(r2.x(), r2.y()-line_width, r2.x(), r2.y()+line_width);
		}
	}

	/*
	 * Draw surrounding box
	 */
	auto box_ptr = simulation->box();
	auto& b = *box_ptr;
	//Box b(simulation->params()->box_dimension);
	for (int i=0; i<4; ++i) {
		QPointF v1(b.vertices[i].x, b.vertices[i].y);
		QPointF v2(b.vertices[(i+1)%4].x, b.vertices[(i+1)%4].y);
		v1 = m*v1;
		v2 = m*v2;
		painter.drawLine(v1, v2);
	}

	// tell qt that we start painting directly to the opengl context
	painter.beginNativePainting();
	// forward paintEvent to superclass which will call paintGL
	// note that since we already painted with QPaint the camera 
	//  and viewport is already setup by qt
	// note that by default QOpenGLWidget::paintEvent would clear
	//  the framebuffer, which we explictly do not want since
	//  we already painted something. To prevent this 
	//  setUpdateBehavior(QOpenGLWidget::PartialUpdate) was used
	QOpenGLWidget::paintEvent(e);
	// tell qt that we finished painting to the opengl context
	painter.endNativePainting();

	// issue next frame
    QTimer::singleShot(1000/60, this, SLOT(update()));
}

void OpenGLWidget::paintGL() {
	glScalef(viewport.zoom, -viewport.zoom, 1);

	glClearColor(255, 255, 255, 0);
	glColor3f(0, 0, 0);
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /*
	 * Draw phase_space
	 */
	
	// get shared pointer on the phase space
	//  note that this is required since changing the phase_space object
	//  while rendering could otherwise lead to memory corruption if the
	//  memory of the phase space stored in the shared pointer is freed
	auto phase_space_ptr = simulation->phase_space(true);
	if (phase_space_ptr) {
		// get actual instance to avoid shared_ptr overhead
		auto phase_space = *phase_space_ptr;
		auto particle_radius = simulation->params()->particle_radius;

		for (unsigned i=0; i<phase_space.size(); ++i) {
			//std::cout << "particle drawed" << std::endl;
			draw_particle(phase_space.x[i], phase_space.y[i], particle_radius, phase_space.color[i]);
		}
	}

	glFlush();
}

void OpenGLWidget::updateCamera() {
  //glMatrixMode(GL_PROJECTION);
  //glLoadIdentity();
  //glOrtho(viewport.x0, viewport.x0+viewport.width, 
  //        viewport.y0, viewport.y0+viewport.height, 1, -1);
  //glScalef(viewport.zoom, viewport.zoom, 1);
}

void OpenGLWidget::mousePressEvent(QMouseEvent* e) {
	mouse.drag = true;
	mouse.drag_x = e->x();
	mouse.drag_y = e->y();
}

void OpenGLWidget::mouseMoveEvent(QMouseEvent* e) {
	if(mouse.drag) {
		int x = e->x();
		int y = e->y();
		viewport.x0 -= x - mouse.drag_x;
		viewport.y0 += y - mouse.drag_y;
		mouse.drag_x = x;
		mouse.drag_y = y;
		//updateCamera();

		//std::cout << viewport.x0 << " " << viewport.y0 << std::endl;
 	}
}

void OpenGLWidget::mouseReleaseEvent(QMouseEvent*) {
	mouse.drag = false;
}

} // end ns Visualizer