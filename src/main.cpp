#include <iostream>
#include <vector>
#include <cmath>
#include "types.hpp"
#include "simulation.hpp"
#include "Parameters.hpp"
#include "PhaseSpace.hpp"
#include "box.hpp"

int main() {
	// read in parameters
	auto params = Parameters::read("../params.json");

	// setup system
    PhaseSpace phase_space = PhaseSpaceFactory::create(params.box_dimension, params.particle_radius);
	Box b(params.box_dimension);
	//initialize_boundary(b,params.particle_radius, box_dimension);

	// store intial configuration to file
	std::ofstream initial_configuration_fh;
	initial_configuration_fh.open("initial-configuration.dat");
	initial_configuration_fh << phase_space;
	initial_configuration_fh.close();

	//simulation(phase_space,params);
	for (int i = 0; i < phase_space.x.size(); ++i) {
		std::cout << phase_space.x[i] << " " << phase_space.v[i] << std::endl;
	}

	// for i
	//	for j
	//	  calculate ghost particles and their velocity
	//    calculate collisions
	//	  for each collision
	return 0;
}