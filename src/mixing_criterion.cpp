#include <iostream>
#include "Parameters.hpp"
#include "PhaseSpace.hpp"

#include "mixing_criterion.hpp"



int main () {

	auto params = Parameters::read("../params.json");
	params.box_dimension = 256;
    PhaseSpace phase_space = PhaseSpaceFactory::create(params.box_dimension, params.particle_radius);
	Box b(params.box_dimension);
	compute_mixing_criterion(phase_space,b);
	return 0;
}