#include "simulation.hpp"
#include "gear_pred_corr.hpp"
#include <iostream>
#include <cmath>
#include <tuple>
#include "utilities.hpp"
#include "box.hpp"
#include "collision.hpp"
#include <chrono>
#include <thread>
#include "utilities.hpp"
#include <cassert>
#include <omp.h>




std::tuple<calc_t,calc_t> contact_force (const calc_t x1, const calc_t y1, const calc_t x2, const calc_t y2, 
                                        const calc_t u1, const calc_t v1, const calc_t u2, const calc_t v2,
                                        Parameters& params, Collision& collision) {


    const calc_t particle_mass = params.density*4.0/3.0*M_PI*std::pow(params.particle_radius, 3);

    calc_t dist = distance(x1,y1,x2,y2);
    calc_t xin = 2 * params.particle_radius - dist;
    calc_t enx = (x2 - x1) / dist;
    calc_t eny = (y2 - y1) / dist;
    calc_t etx = -eny;
    calc_t ety = enx;
    calc_t k = std::sqrt(params.particle_radius * xin);
    calc_t kn = 4.0 / 3.0 * params.E * k;
    calc_t ks = 8.0 * params.G * k;

    calc_t vt_rel = (u2-u1)*etx + (v2-v1)*ety;
    calc_t xit = collision.vt_rel + params.h*vt_rel;

    calc_t fnx = -kn * xin * enx - params.gamma * std::sqrt(xin) * enx * (xin - collision.xin)/params.h; 
    calc_t fny = -kn * xin * eny - params.gamma * std::sqrt(xin) * eny * (xin - collision.xin)/params.h;

    calc_t fn_norm = std::sqrt(fnx*fnx + fny*fny);
    calc_t ftx = etx * std::min(2.0/3.0*ks*xit,params.gamma*fn_norm);
    calc_t fty = ety * std::min(2.0/3.0*ks*xit,params.gamma*fn_norm);

    calc_t fx = fnx+ftx;
    calc_t fy = fny+fty; 

    collision.update_collision(xin,xit);

    fx /= particle_mass;
    fy /= particle_mass;

    return std::make_tuple(fx,fy);
};


void change_velocity_on_boundary(const calc_t x, const calc_t y, calc_t& u, calc_t& v,  calc_t& fx,  calc_t& fy, Box& b, Parameters& params) {
    const calc_t damping = 1;
    calc_t fx_tmp = 0;
    calc_t fy_tmp = 0;
    for (unsigned i = 0; i < 4; ++i) {
        calc_t dist = b.distance_to_edge(x,y,b.vertices[i],b.normals[i]);
        if (dist < params.particle_radius) {
            calc_t dot_prod = u*b.normals[i](0) + v*b.normals[i](1);
            Collision c(100000,1000000,2*params.particle_radius-2*dist);
            calc_t v_norm = std::sqrt(u*u+v*v);
            std::tie(fx_tmp,fy_tmp) = contact_force(x,y,
                x+2*dist*b.normals[i](0),
                y+2*dist*b.normals[i](1),
                u,
                v,
                u-2*dot_prod*b.normals[i](0),
                v-2*dot_prod*b.normals[i](1),params,c);
            fx += fx_tmp;
            fy += fy_tmp;


            u = damping*(u - 2*dot_prod*b.normals[i](0));
            v = damping*(v - 2*dot_prod*b.normals[i](1));
        
        } 


    }
}




void simulation (Simulation& simulation) {
    // acquire shared ownership
    // note: do not dereference these directly since otherwise 
    //  reseting the shared pointers in the gui will free the
    //  objects while they are still in use here

    auto phase_space_ptr = simulation.phase_space();
    auto ghost_space_ptr = simulation.ghost_space();
    auto params_ptr = simulation.params();

    auto& phase_space = *phase_space_ptr;
    auto& ghost_space = *ghost_space_ptr;
    auto& params = *params_ptr;


    bool cell_lists_activated = false;
    bool box_rotation_activated = true;

    std::cout << "simulation started" << std::endl;

	calc_t t = 0.0;
    calc_t speed = 90*params.h * M_PI / 180;
    const calc_t h = params.h;
    const calc_t g = 9.81;
    const calc_t particle_mass = params.density*4.0/3.0*M_PI*std::pow(params.particle_radius, 3);
    const calc_t moment_inertia = particle_mass*std::pow(params.particle_radius,2);
    Box& b = *simulation.box();
    auto& x = phase_space.x;
    auto& y = phase_space.y;
    auto& u = phase_space.u;
    auto& v = phase_space.v;
    auto& rotation = phase_space.rotation;

    const unsigned size = phase_space.size();

    //add particles to the different Cell Lists
    if (cell_lists_activated) {
        for (int i = 0; i < size; ++i) {
            b.grid.push_back(x[i],y[i],i);
        }
    }
    std::vector<calc_t> fx(size,0.0);
    std::vector<calc_t> fx_tm1(size,0.0);
    std::vector<calc_t> fx_tm2(size,0.0);
    std::vector<calc_t> fx_pred(size,0.0);

    std::vector<calc_t> fy(size,-g);
    std::vector<calc_t> fy_tm1(size,-g);
    std::vector<calc_t> fy_tm2(size,-g);
    std::vector<calc_t> fy_pred(size,-g);

    std::vector<calc_t> moment(size,0.0);
    std::vector<calc_t> moment_tm1(size,0.0);
    std::vector<calc_t> moment_tm2(size,0.0);
    std::vector<calc_t> rot_velocity(size,0.0);

    std::vector<calc_t> x_pred(size,0.0);
    std::vector<calc_t> y_pred(size,0.0);
    std::vector<calc_t> u_pred(size,0.0);
    std::vector<calc_t> v_pred(size,0.0);
    std::vector<calc_t> rotation_pred(size,0.0);
    std::vector<calc_t> rot_velocity_pred(size,0.0);
    std::vector<calc_t> moment_pred(size,0.0);

    CollisionList collision_list(size);
    CollisionList boundary_collision_list(10*size);

    calc_t dist, xin;
    calc_t fx_tmp, fy_tmp;

    while(true) {
        if (simulation.stop_requested()) {
            break;
        }

        // treatment of boundary collisions (not fully functional right now)
        /*for (unsigned i = 0; i < size; ++i) {
                //check for collision between particle and boundary and reflect particle on boundary
                if (b.is_outside(x[i],y[i],params.particle_radius)) {
                    //change_velocity_on_boundary(x[i],y[i],u[i],v[i],fx[i], fy[i],b, params);

                    for (unsigned j = 0; j < 4; ++j) {
                        // check if we already have a collision with the boundary
                        bool already_colliding = false;
                        for (Collision& c : boundary_collision_list[i]) {
                            if (c.border == j) {
                                already_colliding = true;
                                //std::cout << "particle " << i << " is already colliding with border" << c.border;
                            }
                        }
                        if (already_colliding) {
                            continue;
                        }

                        calc_t xi = x[i];
                        calc_t yi = y[i];
                        calc_t dist = std::abs(b.distance_to_edge(xi,yi,b.vertices[j],b.normals[j]));
                        if (dist < params.particle_radius)  {
                            // calculate boundary particle collisions
                            calc_t x_new = xi+2*dist*b.normals[j](0);
                            calc_t y_new = yi+2*dist*b.normals[j](1);

                            // calculate boundary particle velocities
                            calc_t dot_prod = u[i]*b.normals[i](0) + v[i]*b.normals[i](1);
                            calc_t ub = (u[i] - 2*dot_prod*b.normals[i](0));
                            calc_t vb = (v[i] - 2*dot_prod*b.normals[i](1));

                            calc_t dist2 = distance(xi,yi,x_new,y_new);
                            calc_t xin = 2 * params.particle_radius - dist2;

                            ghost_space.add_particle(x_new, y_new, ub, vb);
                            boundary_collision_list.add_collision(i, phase_space.size()+ghost_space.size()-1, xin);
                        }
                    }
                }
            }*/


        //check for collision between particle and boundary and reflect particle on boundary
        #pragma omp parallel for
        for (unsigned i = 0; i < size; ++i) {
            if (b.is_outside(x[i],y[i],params.particle_radius)) {
                change_velocity_on_boundary(x[i],y[i],u[i],v[i],fx[i], fy[i],b, params);
            }
        }

        //check for collision between particles and particles and insert into collision_list
        if (cell_lists_activated) {
            b.grid.add_collisions(x,y,collision_list,params.particle_radius);
        } 
        else { 
            #pragma omp parallel for collapse(2)        
            for (unsigned i = 0; i < size; ++i) {
                for (unsigned j = 0; j < size; ++j) {
                    if (i != j) {
                        dist = distance(x[i],y[i],x[j],y[j]);
                        xin = 2 * params.particle_radius - dist;
                        if (xin > 0) {
                            #pragma omp critical 
                            {
                            collision_list.add_collision(i,j,xin);
                            }
                        }
                    }
                }
            }
        }

        //compute forces of colliding particles
        #pragma omp parallel for
        for (unsigned i = 0; i < size; ++i) {
            for (auto it = collision_list[i].begin(); it != collision_list[i].end(); ++it) {
                assert(i != it->j);
                std::tie(fx_tmp,fy_tmp) = contact_force(x[i],y[i],x[it->j],y[it->j],u[i],v[i],u[it->j],v[it->j],params,*it);
                fx[i] += fx_tmp;
                fy[i] += fy_tmp;
                moment[i] += ((x[i]-x[it->j])*fx[i] + (y[i]-y[it->j])*fy[i]) / moment_inertia;
            }
        }


        #pragma omp parallel for
        //compute predicted position of all particles
        for (unsigned i = 0; i < size; ++i) {
            x_pred[i] = x[i] + h*(u[i] + h*(19./24.*fx[i] - 10./24.*fx_tm1[i] + 3./24.*fx_tm2[i]));
            u_pred[i] = (x_pred[i]-x[i])/h + h*(27./24.*fx[i] - 22./24.*fx_tm1[i] + 7./24.*fx_tm2[i]);
            y_pred[i] = y[i] + h*(v[i] + h*(19./24.*fy[i] - 10./24.*fy_tm1[i] + 3./24.*fy_tm2[i]));
            v_pred[i] = (y_pred[i]-y[i])/h + h*(27./24.*fy[i] - 22./24.*fy_tm1[i] + 7./24.*fy_tm2[i]);

            rotation_pred[i] = rotation[i] + h*(rot_velocity[i] + h*(19./24.*moment[i] - 10./24.*moment_tm1[i] + 3./24.*moment_tm2[i]));
            rot_velocity_pred[i] = (rotation_pred[i]-rotation[i])/h + h*(27./24.*moment[i] - 22./24.*moment_tm1[i] + 7./24.*moment_tm2[i]);
        }

        //update cell lists based on predicted positions
        if (cell_lists_activated) {
            b.grid.update(x_pred,y_pred);
        }


        //remove collisions from collision_list if they are not colliding anymore based on predicted positions
        #pragma omp parallel for
        for (unsigned i = 0; i < size; ++i) {
            collision_list[i].remove_if(
            [&] (Collision c) {
                dist = distance(x_pred[i],y_pred[i],x_pred[c.j],y_pred[c.j]);
                return ((2 * params.particle_radius - dist) < 0);
            });
        } 


        //check for collision between particle and boundary and reflect particle on boundary
        #pragma omp parallel for
        for (unsigned i = 0; i < size; ++i) {
            if (b.is_outside(x_pred[i],y_pred[i],params.particle_radius)) {
                change_velocity_on_boundary(x_pred[i],y_pred[i],u_pred[i],v_pred[i],fx_pred[i], fy_pred[i],b, params);
            }
        }


        //check for collision between particles and particles and insert into collision_list
        if (cell_lists_activated) {
            b.grid.add_collisions(x_pred,y_pred,collision_list,params.particle_radius);
        } else {
            #pragma omp parallel for collapse(2)
            for (unsigned i = 0; i < size; ++i) {
                for (unsigned j = 0; j < size; ++j) {
                    if (i != j) {
                        dist = distance(x_pred[i],y_pred[i],x_pred[j],y_pred[j]);
                        xin = 2 * params.particle_radius - dist;
                        if (xin > 0) {
                            #pragma omp critical 
                            {
                            collision_list.add_collision(i,j,0);
                            }
                        }
                    }
                }
            }
        }



        //compute forces of colliding particles
        #pragma omp parallel for
        for (unsigned i = 0; i < size; ++i) {
            for (auto it = collision_list[i].begin(); it != collision_list[i].end(); ++it) {
                std::tie(fx_tmp,fy_tmp) = contact_force(x_pred[i],y_pred[i],x_pred[it->j],y_pred[it->j],u_pred[i],v_pred[i],u_pred[it->j],v_pred[it->j],params,*it);
                assert(i != it->j);
                fx_pred[i] += fx_tmp;
                fy_pred[i] += fy_tmp;
                moment_pred[i] += ((x_pred[i]-x_pred[it->j])*fx_pred[i] + (y_pred[i]-y_pred[it->j])*fy_pred[i]) / moment_inertia;
            }
        }


        //compute new position of particles
        #pragma omp parallel for
        for (unsigned i = 0; i < size; ++i) {

            const calc_t x_tmp = x[i];
            const calc_t y_tmp = y[i];
            const calc_t rotation_tmp = rotation[i];

            // correct position and velocity
            x[i] = x_tmp+h*(u[i]+h*(3./24.*fx_pred[i] + 10./24.*fx[i] - 1./24.*fx_tm1[i]));
            u[i] = (x[i]-x_tmp)/h+h*(7./24.*fx_pred[i] + 6./24.*fx[i] - 1./24.*fx_tm1[i]);
            y[i] = y_tmp+h*(v[i]+h*(3./24.*fy_pred[i] + 10./24.*fy[i] - 1./24.*fy_tm1[i]));
            v[i] = (y[i]-y_tmp)/h+h*(7./24.*fy_pred[i] + 6./24.*fy[i] - 1./24.*fy_tm1[i]);

            rotation[i] = rotation_tmp+h*(rot_velocity[i]+h*(3./24.*moment_pred[i] + 10./24.*moment[i] - 1./24.*moment_tm1[i]));
            rot_velocity[i] = (rotation[i]-rotation_tmp)/h+h*(7./24.*moment_pred[i] + 6./24.*moment[i] - 1./24.*moment_tm1[i]);
        }

        //update cell lists
        if (cell_lists_activated) {
            b.grid.update(x,y);
        }

        //remove collisions from collision_list if they are not colliding anymore based on predicted positions
        #pragma omp parallel for
        for (unsigned i = 0; i < size; ++i) {
            collision_list[i].remove_if(
            [&] (Collision c) {
                dist = distance(x[i],y[i],x[c.j],y[c.j]);
                return ((2 * params.particle_radius - dist) < 0);
            });
        } 
         
        // update force history
        std::swap(fx_tm2,fx_tm1);
        std::swap(fx_tm1,fx);
        std::swap(fy_tm2,fy_tm1);
        std::swap(fy_tm1,fy);
        std::swap(moment_tm2,moment_tm1);
        std::swap(moment_tm1,moment);


        std::fill(fx.begin(),fx.end(),0.0);
        std::fill(fy.begin(),fy.end(),-g);//apply gravity
        std::fill(fx_pred.begin(),fx_pred.end(),0.0);
        std::fill(fy_pred.begin(),fy_pred.end(),-g); //apply gravity
        std::fill(moment.begin(),moment.end(),0.0);
        std::fill(moment_pred.begin(),moment_pred.end(),0.0);

        t += params.h;
        simulation.stepped(t);
        if (box_rotation_activated) {
            b.turn(speed);
        }
    }
    std::cout << "simulation stopped" << std::endl;
}
