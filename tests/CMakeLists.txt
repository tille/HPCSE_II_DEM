CMAKE_MINIMUM_REQUIRED(VERSION 3.1)

enable_testing()

file(GLOB test_files *.cpp)
add_executable(all_tests ${test_files} all_tests.cpp)
add_test(NAME all_tests COMMAND all_tests)
