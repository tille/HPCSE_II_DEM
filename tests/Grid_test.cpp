#include "catch.hpp"
#include "box.hpp"
#include "PhaseSpace.hpp"
#include "types.hpp"

TEST_CASE("Test if particles are correctly placed in grid","[Grid Test]") {

	calc_t dimension = 10;
	Box b(dimension);

	SECTION("single particle", "[single_particle]") {
	
		PhaseSpace phase_space(1);
		phase_space.add_particle(5,0);
		for (int i = 0; i < phase_space.size(); ++i) {
			b.grid.push_back(phase_space.x[i],phase_space.y[i],i);
		}

		for (unsigned i = 0; i < b.grid.cells.size(); ++i) {
			if (i != 13) {
				REQUIRE(b.grid.cells[i].particles.empty());
			}
			else {
				REQUIRE(b.grid.cells[i].particles.size() == 1);
			}
		}
	}

	SECTION("single particle moved", "[single_particle_moved]") {
	
		PhaseSpace phase_space(1);
		phase_space.add_particle(5,0);
		for (int i = 0; i < phase_space.size(); ++i) {
			b.grid.push_back(phase_space.x[i],phase_space.y[i],i);
		}

		for (unsigned i = 0; i < b.grid.cells.size(); ++i) {
			if (i != 13) {
				REQUIRE(b.grid.cells[i].particles.empty());
			}
			else {
				REQUIRE(b.grid.cells[i].particles.size() == 1);
			}
		}

		phase_space.x[0] = -5;
		b.grid.update(phase_space.x, phase_space.y);

		for (unsigned i = 0; i < b.grid.cells.size(); ++i) {
			if (i != 11) {
				REQUIRE(b.grid.cells[i].particles.empty());
			}
			else {
				REQUIRE(b.grid.cells[i].particles.size() == 1);
			}
		}
	}

}

TEST_CASE("find particles in neighboring cells","[neighbors]") {

	calc_t dimension = 20;
	Box b(dimension);
	

	SECTION("two particles overlapping in same cell", "[same cell overlapping]") {
		PhaseSpace phase_space(2);
		CollisionList collision_list(2);
		phase_space.add_particle(-7,-8);
		phase_space.add_particle(-8,-8);
		for (int i = 0; i < phase_space.size(); ++i) {
			b.grid.push_back(phase_space.x[i],phase_space.y[i],i);
		}

		REQUIRE(collision_list.find_collision(0,1) == false);
		b.grid.add_collisions(phase_space.x, phase_space.y,collision_list,1.0);
		REQUIRE(collision_list.find_collision(0,1) == true);
	}

	SECTION("two particles not overlapping in same cell", "[same cell not overlapping]") {
		PhaseSpace phase_space(2);
		CollisionList collision_list(2);
		phase_space.add_particle(-6.5,-8);
		phase_space.add_particle(-9,-8);
		for (int i = 0; i < phase_space.size(); ++i) {
			b.grid.push_back(phase_space.x[i],phase_space.y[i],i);
		}

		REQUIRE(collision_list.find_collision(0,1) == false);
		b.grid.add_collisions(phase_space.x,phase_space.y,collision_list,1.0);
		REQUIRE(collision_list.find_collision(0,1) == false);
	}


	SECTION("two particles overlapping in different cells", "[different cell overlapping]") {
		PhaseSpace phase_space(2);
		CollisionList collision_list(2);
		phase_space.add_particle(-6.5,-8);
		phase_space.add_particle(-5.5,-8);
		for (int i = 0; i < phase_space.size(); ++i) {
			b.grid.push_back(phase_space.x[i],phase_space.y[i],i);
		}

		REQUIRE(collision_list.find_collision(0,1) == false);
		b.grid.add_collisions(phase_space.x,phase_space.y,collision_list,1.0);
		REQUIRE(collision_list.find_collision(0,1) == true);
	}

	SECTION("two particles not overlapping in different cells", "[different cell not overlapping]") {
		PhaseSpace phase_space(2);
		CollisionList collision_list(2);
		phase_space.add_particle(-5.5,-8);
		phase_space.add_particle(-9,-8);
		for (int i = 0; i < phase_space.size(); ++i) {
			b.grid.push_back(phase_space.x[i],phase_space.y[i],i);
		}

		REQUIRE(collision_list.find_collision(0,1) == false);
		b.grid.add_collisions(phase_space.x,phase_space.y,collision_list,1.0);
		REQUIRE(collision_list.find_collision(0,1) == false);
	}

}