#include "types.hpp"
#include "catch.hpp"
#include "box.hpp"

TEST_CASE("Test if particle is inside or outside box", "[OutsideBox_test]") {

	calc_t particle_radius = 1.0;
	calc_t box_dimension = 10.0;
	Box b(box_dimension);

	SECTION("distance_to_edge") {
		REQUIRE(b.distance_to_edge(0.,0., b.vertices[0], b.normals[0]) == box_dimension/2.);
		REQUIRE(b.distance_to_edge(0, - box_dimension/2., b.vertices[0], b.normals[0]) == box_dimension/2.);
	}

	SECTION("distance_to_border") {
		REQUIRE(b.distance_to_border(0.,0.) == box_dimension/2.);
		REQUIRE(b.distance_to_border(box_dimension/2.,0) == 0.);
		REQUIRE(b.distance_to_border(box_dimension/2., - box_dimension/2.) == 0.);
		REQUIRE(b.distance_to_border(0.6 * box_dimension/2., 0.8* box_dimension/2.) == 0.2 * box_dimension/2.);
	}

	SECTION("is_outside") {
		REQUIRE (b.is_outside(0.0, 0.0, particle_radius) == false);
		REQUIRE (b.is_outside(1.0, 2.0, particle_radius) == false);
		REQUIRE (b.is_outside(0.0,4.0, particle_radius) == false);
		REQUIRE (b.is_outside(-4.0,-4.0, particle_radius) == false);
		REQUIRE (b.is_outside(15,20,particle_radius) == true);
		REQUIRE (b.is_outside(4.5,0.0, particle_radius) == true);
		REQUIRE (b.is_outside(4.5,-4.5, particle_radius) == true);
	}

	SECTION("is_outside (after rotation by 30 degrees)") {
		b.turn(0.*M_PI/180.);
		REQUIRE (b.is_outside(0.0, 0.0, particle_radius) == false);
		REQUIRE (b.is_outside(1.0, 2.0, particle_radius) == false);
		REQUIRE (b.is_outside(0.0,4.0, particle_radius) == false);
		REQUIRE (b.is_outside(-4.0,-4.0, particle_radius) == false);
		REQUIRE (b.is_outside(15,20,particle_radius) == true);
		REQUIRE (b.is_outside(4.5,0.0, particle_radius) == true);
		REQUIRE (b.is_outside(4.5,-4.5, particle_radius) == true);
	}
}
