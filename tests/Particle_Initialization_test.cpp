#include "catch.hpp"
#include "types.hpp"
#include "box.hpp"
#include "PhaseSpace.hpp"


TEST_CASE("Correct initialization in PhaseSpaceFactory", "[Particle_Initialization_test]") {

	calc_t particle_radius = 1.0;
	calc_t box_dimension = 20.0;
	Box b(box_dimension);
	PhaseSpace phase_space = PhaseSpaceFactory::create(box_dimension, particle_radius);


	SECTION("all particles are inside the box") {
		for (unsigned i = 0; i < phase_space.size(); ++i) {
			INFO("particle ID: " << i << " " << phase_space.x[i] << " " << phase_space.y[i]);
			REQUIRE(b.is_outside(phase_space.x[i], phase_space.y[i], particle_radius) == false);
		}
	}

	SECTION("volume of all particles is less than half of the box volume") {
		calc_t volume_particles = phase_space.size() * M_PI * std::pow(particle_radius,2);
		calc_t volume_box = std::pow(box_dimension,2);
		REQUIRE(volume_particles <= volume_box/2.);
	}

	SECTION("no particles are overlapping") {
		for (unsigned i = 0; i < phase_space.size(); ++i) {
			for (unsigned j = i+1; j < phase_space.size(); ++j) {
				calc_t distance = std::sqrt( std::pow(phase_space.x[i]-phase_space.x[j],2) + std::pow(phase_space.y[i]-phase_space.y[j],2));
				REQUIRE(distance >= 2*particle_radius -1e-8); //added small correction term because of double imprecision
			}
		}
	}
}