#include <iostream>
#include "catch.hpp"
#include "types.hpp"
#include "box.hpp"
#include "PhaseSpace.hpp"
#include "gear_pred_corr.hpp"
#include "Parameters.hpp"

const calc_t g = 9.81;

TEST_CASE("Single particle") {

	auto params = Parameters::read("../params.json");
	calc_t particle_radius = 1.0;
	calc_t box_dimension = 10.0;
	Box b(box_dimension);

	SECTION("no initial velocity, just gravity","[no_init_velocity]") {

		PhaseSpace phase_space(1);
		phase_space.add_particle(0.0, 0.0);
		calc_t& x = phase_space.x[0];
		calc_t& y = phase_space.y[0];
		calc_t& v = phase_space.v[0];

		PhaseSpace ghost_space(1);

		calc_t t = 0.0;
		const calc_t h = 1e-3;

		calc_t f = -g;
		calc_t f_tm1 = -g;
		calc_t f_tm2 = -g;
		calc_t f_ghost = 0;
		calc_t f_tm1_ghost = 0;
		calc_t f_tm2_ghost = 0;

        bool collision = false;

		for (unsigned iteration = 0; iteration < 10000; ++iteration) {


			calc_t dist;
			calc_t xin;

			auto contact_force = [&dist, &params,&particle_radius] (const calc_t y1, const calc_t y2, calc_t xin) {
			calc_t exn = 0.0;
			calc_t eyn = (y2 - y1)/dist;
			calc_t ext = -eyn;
			calc_t eyt = exn;
            calc_t k = std::sqrt(particle_radius * xin);
            calc_t kn = 4.0/3.0 * params.E * k;
            calc_t ks = 8.0 * params.G * k;
            calc_t xit = 0.0; //TODO

            calc_t forcen(-kn*xin*eyn - params.gamma*std::sqrt(xin) * eyn); //TODO
           
     

			return forcen;
	};

			auto force = [&collision, &ghost_space, &xin, &contact_force] (const calc_t y, const calc_t w, const calc_t t) -> calc_t {
				if (!collision) return -g;
				else return contact_force(y, ghost_space.y.back(), xin) -g;
			};

			auto force_ghost = [&collision, &ghost_space, &xin, &contact_force] (const calc_t y, const calc_t w, const calc_t t) -> calc_t {
				return contact_force(ghost_space.y.back(), y, xin);
			};





			if (b.is_outside(x,y, particle_radius)) {
                if (collision == false) {
                    ghost_space.add_particle(0.0, -(y - ((y > 0) - (y < 0)) * box_dimension), 0.0, -v);
                }
                dist = std::abs(y - ghost_space.y.back());
				xin = 2*particle_radius - dist;

                collision = true;
			}
			else {
				collision = false;
			}

			f = force(y,v,t);
			f_ghost = force(ghost_space.y[0],ghost_space.v[0],t);
			gear_pred_corrector(y,v, t, h, f, f_tm1, f_tm2, force);
			//gear_pred_cciaoorrector(ghost_space.y.back(), ghost_space.v.back(), t, h, f_ghost, f_tm1_ghost, f_tm2_ghost, force_ghost);
            if (iteration > 905) {
                int a = 2;
            }
		}
	}
}
