#include "catch.hpp"
#include "types.hpp"
#include "gear_pred_corr.hpp"

TEST_CASE("Test if Gear predictor corrector scheme is correct for simple problem", "[gear_pred_corr_test]") {

	const calc_t h = 1e-2;
	const calc_t epsilon = 1e-8;

	SECTION("constant acceleration") {

		calc_t x = 0;
	    calc_t v = 0;
	    auto force = [] (const calc_t x, const calc_t v, const calc_t t) -> calc_t {
	        return 1;
	    };

	    // store force values
	    calc_t f;
	    calc_t f_tm1 = 1;
	    calc_t f_tm2 = 1;

	    calc_t t=0;
	    for (unsigned i=0; i<100; ++i) {
	        // evaluate force
	        f = force(x, v, t);
	        // timestepping
	        gear_pred_corrector(x, v, t, h, f, f_tm1, f_tm2, force);
	        t+=h;
	        REQUIRE(std::abs(x - 0.5*t*t) < epsilon);
	        REQUIRE(std::abs(v - t) < epsilon);
	    }
	}

}
